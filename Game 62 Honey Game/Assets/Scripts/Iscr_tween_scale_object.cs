﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Iscr_tween_scale_object 
{
    void Enlarge_Info_Tween();
    void Reduce_Info_Tween();

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class scr_check_answer : MonoBehaviour
{
    [SerializeField]scr_yellow_screen ref_yellow_screen;
    [SerializeField]scr_audio_manager ref_audio_manager;    
    [SerializeField]scr_game_condition ref_game_condition;
    [SerializeField]scr_card_manager ref_card_manager;
    [SerializeField]scr_drop ref_drop;
    [SerializeField]scr_ui_popup_function ref_ui_popup;
    [SerializeField]scr_outcome ref_outcome;
    [SerializeField]GameObject x_mark_skate_card;
    [SerializeField]scr_x_mark_guess ref_yellow_x;
    [SerializeField]int[] x_placements;
    public int[] Get_X_Placement
    {
        get{return x_placements;}
    }
    [SerializeField]bool is_repeated = false;

    void Awake()
    {
        ref_game_condition = this.GetComponent<scr_game_condition>();
        ref_outcome = this.GetComponent<scr_outcome>();
        ref_card_manager = FindObjectOfType<scr_card_manager>();
        ref_audio_manager = GameObject.FindObjectOfType<scr_audio_manager>();
        ref_yellow_screen = FindObjectOfType<scr_yellow_screen>();
        
    }


    public void Check_Answers(PointerEventData eventData,int the_correct_answer, int card_id,int data_choice_value)
    {   

        ref_drop.Prop_IsLocked_Place = false;
        if(ref_yellow_x.Prop_X_Mark_Yellow[0].activeSelf == true)
        {
            ref_yellow_x.Prop_X_Mark_Yellow[0].SetActive(false);
        }
        // Set Draggable to true, Allow Answer Cards to drag again
        for(int i = 0; i < ref_card_manager.Prop_Answer_Choice_Cards.Length; i++)
        {
            ref_card_manager.Prop_Answer_Choice_Cards[i].GetComponent<scr_drag_drop_event_system>().Prop_Is_Draggable = true;
        }
        // Set the Script Active
        ref_card_manager.Prop_Array_Cards[card_id].button.GetComponent<scr_tween_shake_rotate_cards>().Shake_Tween();
        //WHEN THE PLAYER ANSWERED IT CORRECTLY
        // Checks if evenData.pointerDrag matches the ref_Angle_choice_card with the correct answer
        if(eventData.pointerDrag == ref_card_manager.Prop_Answer_Choice_Cards[the_correct_answer])
        {   
            Debug.Log("Correct: "+ the_correct_answer +"    eventData: " + eventData.pointerDrag);
            StartCoroutine(Correct_Answer(card_id, data_choice_value));
            
        }
        else// WHEN THE PLAYER ANSWERED IT INCORRECTLY
        {   
            Debug.Log("Incorrect: " + the_correct_answer +"    eventData: " + eventData.pointerDrag);
            StartCoroutine(Incorrect_Answer(card_id, data_choice_value));
        }
        // GO TO BEE SCREEN
        ref_yellow_screen.Prop_Yellow_Screen_Object.SetActive(false);
        ref_audio_manager.Play_Audio("swoosh_yellow_screen_sfx");
        
    }

    public IEnumerator Correct_Answer(int card_id,int data_choice_value)
    {  
        //Reset Guesses for each correct 
        ref_game_condition.Prop_Guesses = 2;
        for(int i = 0; i < x_placements.Length; i++)
        {
            x_placements[i] = 0;
        }
        if(is_repeated == true)
        is_repeated = false;
        // Set the Array Cards sprite into the correct angle/answer cards
        yield return new WaitForSeconds(1.0f);
        ref_card_manager.Prop_Array_Cards[card_id].card_object.GetComponent<Image>().sprite =
        ref_card_manager.Prop_Array_Cards[card_id].answer_cards_sprite[data_choice_value];
        ref_audio_manager.Play_Audio("ans_appear_bee_sfx");
        
        yield return new WaitForSeconds(0.5f);
        ref_audio_manager.Play_Audio("correct_sfx");
        yield return new WaitForSeconds(0.75f);
        // Play one of the Correct_Vo's after "ans_appear_bee_sfx"
        Cycle_Correct_VO();
        yield return new WaitForSeconds(0.5f);

        ref_audio_manager.Play_Audio("spin_sfx");
        ref_card_manager.Prop_Array_Cards[card_id].button.GetComponent<scr_tween_shake_rotate_cards>().Rotate_Tween();
        yield return new WaitForSeconds(2.0f);
        ref_audio_manager.Play_Audio("fly_away_sfx");
        ref_card_manager.Prop_Array_Cards[card_id].card_object.SetActive(false);
        ref_game_condition.Prop_Num_Answered_Correctly++;
        Set_Array_Card_Buttons_True();
        if(ref_game_condition.Prop_Num_Answered_Correctly >= 12)
        {
            StartCoroutine(ref_outcome.Outcome());
        }


    }

    public IEnumerator Incorrect_Answer(int card_id,int data_choice_value)
    {
        x_placements[ref_yellow_x.Prop_X_Yellow_Index] = data_choice_value;
        // Set the Array Cards sprite into the correct angle/answer cards
        yield return new WaitForSeconds(1.0f);
        ref_card_manager.Prop_Array_Cards[card_id].card_object.GetComponent<Image>().sprite =
        ref_card_manager.Prop_Array_Cards[card_id].answer_cards_sprite[data_choice_value];
        ref_audio_manager.Play_Audio("ans_appear_bee_sfx");

        yield return new WaitForSeconds(0.5f);
        // Play one of the Correct_Vo's after "ans_appear_bee_sfx"
        Cycle_Incorrect_VO();
        // X Mark Appears after a wrong answer
        x_mark_skate_card.SetActive(true);
        x_mark_skate_card.GetComponent<scr_x_mark>().Reduce_Info_Tween();
        // X Mark Appears at specific Card Location
        x_mark_skate_card.GetComponent<RectTransform>().anchoredPosition = ref_card_manager.Prop_Array_Cards[card_id].
        card_object.GetComponent<RectTransform>().anchoredPosition; 
        // Deduct 1 Guesses
        ref_game_condition.Prop_Guesses--;
        yield return new WaitForSeconds(1.5f);
        // Goes to the Yellow Screen from Bee Screen
        if(ref_yellow_screen.Prop_Yellow_Screen_Object.activeSelf == false)
        {
            // Turns off the X Mark Skate when Yellow Screen Appears
            x_mark_skate_card.SetActive(false);
            // Yellow Screen Appears Again for another try
            ref_yellow_screen.Prop_Yellow_Screen_Object.SetActive(true);
            ref_audio_manager.Play_Audio("swoosh_sfx");

            if(ref_game_condition.Prop_Guesses > 0)
            {
                yield return new WaitForSeconds(1.25f);
                ref_audio_manager.Play_Audio("no_guessing_sfx");
                //Record a value to the array for placement memory
                x_placements[ref_yellow_x.Prop_X_Yellow_Index] = data_choice_value;
                if(is_repeated==false)
                {
                    // Warning Pops Up
                    ref_ui_popup.Prop_Warning_UI.SetActive(true);
                    yield return new WaitForSeconds(1.0f);
                    ref_audio_manager.Play_Audio("danger_sfx");
                    is_repeated = true;
                }

                // X Mark appears on top of the Answer Card that is incorrect,is delayed for a bit and Ans Card is undraggable
                ref_yellow_x.Prop_X_Mark_Yellow[ref_yellow_x.Prop_X_Yellow_Index].SetActive(true);
                ref_yellow_x.Prop_X_Mark_Yellow[ref_yellow_x.Prop_X_Yellow_Index].GetComponent<scr_x_mark>().Reduce_Info_Tween();
                // Places the X Mark on top of the Answer Card depending on the choice by the user
                ref_yellow_x.Prop_X_Mark_Yellow[ref_yellow_x.Prop_X_Yellow_Index].GetComponent<RectTransform>().anchoredPosition = 
                ref_card_manager.Prop_Answer_Choice_Cards[data_choice_value].GetComponent<RectTransform>().anchoredPosition;
                // Makes the Answer Card Draggable again
                ref_card_manager.Prop_Answer_Choice_Cards[data_choice_value].GetComponent<scr_drag_drop_event_system>().
                Prop_Is_Draggable = false;
                ref_yellow_x.Prop_X_Yellow_Index++;

            }
            else if(ref_game_condition.Prop_Guesses <= 0)
            {   
                //ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",true);
                // Delay for Transitions
                yield return new WaitForSeconds(0.5f);
                //ref_anim_manager.Prop_H_Transition.SetBool("to_blue_H",false);
                ref_drop.Set_Answer_Cards_Bool_Draggable(false);
                ref_ui_popup.Prop_Warning_UI.SetActive(false);
                StartCoroutine(ref_outcome.Outcome(x_placements));
            }
        }
    }

    public void Cycle_Correct_VO()
    {
        // Play one of the Correct_Vo's after "ans_appear_bee_sfx"
        if(ref_audio_manager.Prop_Correct_Vo_Index >= ref_audio_manager.Prop_Get_Correct_Vo.Length)
        ref_audio_manager.Prop_Correct_Vo_Index = 0;
        // Plays the Audio of Correct_Vo that CYCLES via Correct_Vo_Index
        ref_audio_manager.Play_Audio(ref_audio_manager.Prop_Get_Correct_Vo[ref_audio_manager.Prop_Correct_Vo_Index].name,
        ref_audio_manager.Prop_Get_Correct_Vo);
        // Cycle thru the Correct_Vo Array to switch Audios
        ref_audio_manager.Prop_Correct_Vo_Index++;
    }
    public void Cycle_Incorrect_VO()
    {
        if(ref_audio_manager.Prop_Incorrect_Vo_Index >= ref_audio_manager.Prop_Get_Incorrect_Vo.Length)
        ref_audio_manager.Prop_Incorrect_Vo_Index = 0;
        // Plays the Audio of Correct_Vo that CYCLES via Correct_Vo_Index
        ref_audio_manager.Play_Audio(ref_audio_manager.Prop_Get_Incorrect_Vo[ref_audio_manager.Prop_Incorrect_Vo_Index].name, 
        ref_audio_manager.Prop_Get_Incorrect_Vo);
        // Cycle thru the Correct_Vo Array to switch Audios
        ref_audio_manager.Prop_Incorrect_Vo_Index++;   
    }

    public void Set_Array_Card_Buttons_True()
    {
        for(int i = 0;i < ref_card_manager.Prop_Array_Cards.Length;i++)
        {   // Set all Array Cards buttons to false
            ref_card_manager.Prop_Array_Cards[i].button.enabled = true;
        }
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_tween_shake_rotate_cards : scr_tween_base_object,Iscr_tween_shake_object,Iscr_tween_rotate_object
{

    [SerializeField]private float rotate_degree;
    public float Prop_Rotate_Degree
    {
        get{return rotate_degree;}
        set{rotate_degree = value;}
    }

    [SerializeField]private float rotationZ;
    public float Prop_RotationZ
    {
        get{return rotationZ;}
        set{rotationZ = value;}
    }
    
    [SerializeField]private float rotate_duration;
    public float Prop_Rotate_Duration
    {
        get{return rotate_duration;}
        set{rotate_duration = value;}
    }

    void OnEnable()
    {
        //StartCoroutine(Shake_Tween());
    }
    void Awake()
    {
        ref_audio_manager = FindObjectOfType<scr_audio_manager>();
        tween_object = this.gameObject;
    }

    public void Shake_Tween()
    {
        //yield return new WaitForSeconds(0.0f);
        LeanTween.rotateZ(tween_object,rotate_degree,duration).setLoopPingPong(4);
    }
    public void Rotate_Tween()
    {
        LeanTween.rotateZ(tween_object,rotationZ,rotate_duration);
    }

}

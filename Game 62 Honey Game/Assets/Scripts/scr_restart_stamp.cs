﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_restart_stamp : scr_tween_base_object, Iscr_tween_small_scale
{
    [SerializeField]Vector2 scale_big_goal;
    public Vector2 Prop_Scale_Big_Goal
    {
        get{return scale_big_goal;}
        set{scale_big_goal = value;}
    }

    [SerializeField]Vector2 scale_small_goal;
    public Vector2 Prop_Scale_Small_Goal
    {
        get{return scale_small_goal;}
        set{scale_small_goal = value;}
    }

    void Awake()
    {
        ref_audio_manager = GameObject.FindObjectOfType<scr_audio_manager>();
        tween_object = this.gameObject;
    }

    void OnEnable()
    {
        
    }

    public void Reduce_Info_Tween()
    {
        LeanTween.scale(tween_object,scale_small_goal,duration).setOnComplete(On_Reduce_Complete);
    }

    void On_Reduce_Complete()
    {
        ref_audio_manager.Play_Audio(audio_name);
    }

}
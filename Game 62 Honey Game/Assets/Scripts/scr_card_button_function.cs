﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scr_card_button_function : MonoBehaviour
{
    //[SerializeField]scr_card_button_function ref_card_button_function;
    [SerializeField]scr_card_manager ref_card_manager;
    [SerializeField]scr_audio_manager ref_audio_manager;
    [SerializeField]scr_yellow_screen ref_yellow_manager;
    [SerializeField]scr_drop ref_drop;
    
    void Awake()
    {
        ref_audio_manager = GameObject.FindObjectOfType<scr_audio_manager>();
        
    }

    //When pressed on OnClick Event, It allows yellow screen to be true and sets the Cards and Answer Cards with the scr_card value
    public void Card_Function(int index)
    {
        //Displays the Yellow Screen background
        ref_yellow_manager.Prop_Yellow_Screen_Object.SetActive(true);
        ref_audio_manager.Play_Audio("swoosh_sfx");
        if(index == ref_card_manager.Prop_Array_Cards[index].card_index)
        {   // Test String
            Debug.Log("value: " + ref_card_manager.Prop_Array_Cards[index].card_name);
            //Set the Question Card sprite to the Card Sprite
            ref_card_manager.Prop_Question_Card.sprite = ref_card_manager.Prop_Array_Cards[index].card_sprite;
            for(int i = 0; i < ref_card_manager.Prop_Answer_Choice_Cards.Length; i++)
            {
                ref_card_manager.Prop_Answer_Choice_Cards[i].GetComponent<Image>().sprite =
                ref_card_manager.Prop_Array_Cards[index].answer_cards_sprite[i]; 

                // Set the script ACTIVE
                ref_card_manager.Prop_Answer_Choice_Cards[i].GetComponent<scr_tween_moving_cards>().enabled = true;
            }
            ref_card_manager.Prop_Question_Card.GetComponent<scr_tween_moving_cards>().enabled = true;
            ref_drop.Prop_The_Correct_Answer = (int)ref_card_manager.Prop_Array_Cards[index].answer_Card;
            ref_drop.Prop_Card_Index = ref_card_manager.Prop_Array_Cards[index].card_index;
        }
    }
}

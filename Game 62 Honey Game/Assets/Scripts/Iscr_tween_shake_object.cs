﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Iscr_tween_shake_object 
{
    float Prop_Rotate_Degree{get;set;}
    float Prop_RotationZ{get;set;}
    
    void Shake_Tween();

}

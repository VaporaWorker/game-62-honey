﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_x_mark_guess : MonoBehaviour
{
    [SerializeField]GameObject[] x_marks_yellow;
    public GameObject[] Prop_X_Mark_Yellow
    {
        get{return x_marks_yellow;}
        set{x_marks_yellow = value;}
    }
    [SerializeField] int x_yellow_index;
    public int Prop_X_Yellow_Index
    {
        get{return x_yellow_index;}
        set
        {
            if(x_yellow_index < 3)
            {
                x_yellow_index = value;
            }
            
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Iscr_tween_move_no_delay_object : Iscr_tween_move_object
{
    void Move_Tween_Answer();
}

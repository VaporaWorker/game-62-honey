﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class scr_sound 
{
    public string name;
    public AudioClip clip;
    [Range(0.0f,1.0f)]
    public float volume;
    public bool isLoop;
    public bool isAwake;
    [HideInInspector]
    public AudioSource aud_source;
}

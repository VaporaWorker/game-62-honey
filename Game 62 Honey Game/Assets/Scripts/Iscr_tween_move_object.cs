﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Iscr_tween_move_object
{
    [SerializeField]float Prop_VectorY_Goal{get;set;} 
    [SerializeField]Vector2 Prop_Start_Location{get;set;}

}

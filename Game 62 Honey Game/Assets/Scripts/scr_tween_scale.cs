﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_tween_scale : scr_tween_base_object, Iscr_tween_big_scale, Iscr_tween_small_scale, Iscr_tween_fade_object
{
    [SerializeField] float alpha;
    public float Prop_Alpha
    {
        get{return alpha;}
        set{alpha = value;}
    }
    [SerializeField]float fade_duration;
    public float Prop_Fade_Duration
    {
        get{return fade_duration;}
        set{fade_duration = value;}
    }
    [SerializeField]Vector2 scale_big_goal;
    public Vector2 Prop_Scale_Big_Goal
    {
        get{return scale_big_goal;}
        set{scale_big_goal = value;}
    }

    [SerializeField]Vector2 scale_small_goal;
    public Vector2 Prop_Scale_Small_Goal
    {
        get{return scale_small_goal;}
        set{scale_small_goal = value;}
    }

    void Awake()
    {
        ref_audio_manager = GameObject.FindObjectOfType<scr_audio_manager>();
        tween_object = this.gameObject;
    }

    void OnEnable()
    {
        
    }

    public void Enlarge_Info_Tween()
    {
        LeanTween.scale(tween_object, scale_big_goal,duration).setOnComplete(Fade);
    }
    public void Reduce_Info_Tween()
    {
        LeanTween.scale(tween_object,scale_small_goal,duration).setOnComplete(On_Reduce_Complete).setOnComplete(Fade);
    }

    void On_Reduce_Complete()
    {
        ref_audio_manager.Play_Audio(audio_name);
    }

    public void Fade()
    {   
        LeanTween.alpha(tween_object.GetComponent<RectTransform>(),alpha,fade_duration).setDelay(delay);
    }

    void OnDisable()
    {
        tween_object.SetActive(false);
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class scr_info_button
{
    public string name;
    public int inf_index;
    public bool isDisplayed;
    public Image prompt; 
    public Button button;

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_tween_genius_ui : scr_tween_base_object,Iscr_tween_move_no_delay_object,Iscr_tween_fade_object
{
    [SerializeField] float alpha;
    public float Prop_Alpha
    {
        get{return alpha;}
        set{alpha = value;}
    }
    [SerializeField]float fade_duration;
    public float Prop_Fade_Duration
    {
        get{return fade_duration;}
        set{fade_duration = value;}
    }    [SerializeField] float vectorY_goal;
    public float Prop_VectorY_Goal
    {
        get{return vectorY_goal;}
        set{vectorY_goal = value;}
    }
    [SerializeField] Vector2 start_location;
    public Vector2 Prop_Start_Location
    {
        get{return start_location;}
        set{start_location = value;}
    }
    void Awake()
    {
        ref_audio_manager = GameObject.FindObjectOfType<scr_audio_manager>();
        tween_object = this.gameObject;
    }

    public void Move_Tween_Answer()
    {
        LeanTween.moveLocalY(tween_object,vectorY_goal,duration).setEaseOutBack().setOnComplete(Fade);
    }

    public void Fade()
    {   
        LeanTween.alpha(tween_object.GetComponent<RectTransform>(),alpha,fade_duration).setDelay(delay);
    }

    void OnDisable()
    {
        tween_object.SetActive(false);
    }
    
}

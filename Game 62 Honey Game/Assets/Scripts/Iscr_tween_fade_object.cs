﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Iscr_tween_fade_object
{
    float Prop_Alpha{get;set;}
    float Prop_Fade_Duration{get;set;}
    void Fade();
    
}

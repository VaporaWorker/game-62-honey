﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Iscr_tween_big_scale 
{
    Vector2 Prop_Scale_Big_Goal{get;set;}
    void Enlarge_Info_Tween();
}

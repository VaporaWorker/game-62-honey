﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_tween_info_button : scr_tween_base_object,Iscr_tween_big_scale,Iscr_tween_small_scale 
{
    [SerializeField] private Vector2 scale_big_goal;
    public Vector2 Prop_Scale_Big_Goal
    {
        get{return scale_big_goal;}
        set{scale_big_goal = value;}
    }
    [SerializeField] Vector2 scale_small_goal;
    public Vector2 Prop_Scale_Small_Goal
    {
        get{return scale_small_goal;}
        set{scale_small_goal = value;}
    }
    const float constant_duration = 0.35f;

    void Awake()
    {
        duration = constant_duration;
        tween_object = this.gameObject;
    }

    public void Enlarge_Info_Tween()
    {
        
        LeanTween.scale(tween_object,scale_big_goal,duration);
        
    }

    public void Reduce_Info_Tween()
    {
        LeanTween.scale(tween_object,scale_small_goal,duration);
    }


}

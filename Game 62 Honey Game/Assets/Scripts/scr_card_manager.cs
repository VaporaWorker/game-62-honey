﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scr_card_manager : MonoBehaviour
{
    
    [SerializeField]Image ref_question_card;// The Card that gives out the question
    public Image Prop_Question_Card
    {
        get{return ref_question_card;}
        set{ref_question_card = value;}
    }
    [SerializeField]scr_cards[] array_cards;// An array of Skateboard Cards
    public scr_cards[] Prop_Array_Cards
    {
        get{return array_cards;}
        set{array_cards = value;}
    }
    /*
    [SerializeField]GameObject[] array_cards_object;
    public GameObject[] Prop_Array_Cards_Object
    {
        get{return array_cards_object;}
        set{array_cards_object = value;}
    }
    */
    [SerializeField]Sprite[] array_sprite_cards;
    public Sprite[] Prop_Array_Sprite_Cards
    {
        get{return array_sprite_cards;}
        set{array_sprite_cards = value;}
    }
    [SerializeField]GameObject[] answer_choice_cards;// Needed for te answer cards to change info dynamically
    public GameObject[] Prop_Answer_Choice_Cards
    {
        get{return answer_choice_cards;}
        set{answer_choice_cards = value;}
    }
    [HideInInspector]
    public enum Card_Type
    {
        Skate_Card,
        Question_Card,
        Answer_Card
    }


    void Awake()
    {
        foreach(scr_cards c in array_cards)
        {
            c.ref_audio_manager = GameObject.FindObjectOfType<scr_audio_manager>();
            for(int i = 0; i < array_cards.Length; i++)
            {
                array_sprite_cards[i] = array_cards[i].card_sprite;
                
            }
        }
    }


}

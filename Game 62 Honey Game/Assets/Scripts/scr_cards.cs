﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class scr_cards 
{
    public string card_name;
    public int card_index;
    public Sprite card_sprite;
    public enum answer_card{ONE,TWO,THREE,FOUR}
    public answer_card answer_Card;
    public scr_audio_manager ref_audio_manager;
    public Button button;
    public GameObject card_object;
    public Sprite[] answer_cards_sprite; 
    
    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_yellow_screen : MonoBehaviour
{
    [SerializeField]GameObject yellow_screen_object;//Needed to render the questions

    public GameObject Prop_Yellow_Screen_Object
    {
        get{return yellow_screen_object;}
        set{yellow_screen_object = value;}
    }

}

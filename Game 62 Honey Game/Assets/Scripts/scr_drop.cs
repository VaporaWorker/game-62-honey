﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class scr_drop : MonoBehaviour, IDropHandler
{
    [SerializeField]scr_yellow_screen ref_yellow_screen;
    [SerializeField]scr_card_manager ref_card_manager;
    [SerializeField]scr_audio_manager ref_audio_manager;
    [SerializeField]scr_game_condition ref_game_condition;
    [SerializeField]scr_check_answer ref_check_answer;
    [SerializeField]bool isLocked_in_place = false;
    public bool Prop_IsLocked_Place
    {
       get{return isLocked_in_place;}
       set{isLocked_in_place = value;}
    }
    [SerializeField]int card_id;
    public int Prop_Card_Index
    {
        get{return card_id;}
        set{card_id = value;}
    }
    [SerializeField]int the_correct_answer;// Value given from cards of array for  determining the right answer
    public int Prop_The_Correct_Answer
    {
        get{return the_correct_answer;}
        set{the_correct_answer = value;}
    }
    void Awake()
    {
        ref_audio_manager = GameObject.FindObjectOfType<scr_audio_manager>();
    }

    public void OnDrop(PointerEventData eventData)
    {   
        //Debug.Log("card id: "+ card_id);
        //Debug.Log("eventData: "+ eventData.pointerDrag);
        // Sets the answer card locked to prevent further movement upon being placed on top of the question card
        isLocked_in_place = true;
        var data_choice_value = eventData.pointerDrag.GetComponent<scr_drag_drop_event_system>().Choice_Value;
        ref_card_manager.Prop_Answer_Choice_Cards[data_choice_value].GetComponent<RectTransform>().anchoredPosition = 
        ref_card_manager.Prop_Question_Card.GetComponent<RectTransform>().anchoredPosition;

        // Resets the Answer_Cards to its default canvas group state
        eventData.pointerDrag.GetComponent<CanvasGroup>().alpha = 1f;
        eventData.pointerDrag.GetComponent<CanvasGroup>().blocksRaycasts = true;
        Set_Answer_Cards_Bool_Draggable(false);
        // Set all Array Cards buttons to false
        Set_Array_Card_Buttons_False(); 

        ref_check_answer.Check_Answers(eventData,the_correct_answer,card_id,data_choice_value);

    }
    
    // Prevent Users from Clicking the Cards
    public void Set_Array_Card_Buttons_False()
    {
        for(int i = 0;i < ref_card_manager.Prop_Array_Cards.Length;i++)
        {   // Set all Array Cards buttons to false
            ref_card_manager.Prop_Array_Cards[i].button.enabled = false;
        }
    }

    public void Set_Answer_Cards_Bool_Draggable(bool condition)
    {
        for(int i = 0; i < ref_card_manager.Prop_Answer_Choice_Cards.Length; i++)
        {
            ref_card_manager.Prop_Answer_Choice_Cards[i].GetComponent<scr_drag_drop_event_system>().Prop_Is_Draggable = condition;
        }
    }

}   

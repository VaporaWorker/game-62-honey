﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_tween_moving_cards : scr_tween_base_object, Iscr_tween_move_delay_object,Iscr_playaudio_object
{
    [SerializeField]private float vectorY_goal;
    public float Prop_VectorY_Goal
    {
        get{return vectorY_goal;}
        set{vectorY_goal = value;}
    }   
    [SerializeField]private Vector2 start_location;
    public Vector2 Prop_Start_Location
    {
        get{return start_location;}
        set{start_location = value;}
    }       

    void Awake()
    {
        ref_audio_manager = GameObject.FindObjectOfType<scr_audio_manager>();
    }

    void OnEnable()
    {
        StartCoroutine(Delay_Move_Tween());
        
    }

    public IEnumerator Delay_Move_Tween()
    {
        yield return new WaitForSeconds(delay);
        LeanTween.moveLocalY(tween_object,Prop_VectorY_Goal,duration).setEaseOutBack();
        Play_Tween_Audio();
    }

    public void Play_Tween_Audio()
    {
        ref_audio_manager.Play_Audio(audio_name);
    }

    void OnDisable()
    {
        tween_object.GetComponent<RectTransform>().anchoredPosition = start_location;
    }
}

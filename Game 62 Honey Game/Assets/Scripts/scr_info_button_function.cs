﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class scr_info_button_function : MonoBehaviour
{
    [SerializeField]scr_yellow_screen ref_yellow_screen;
    [SerializeField]scr_info_button[] array_info_buttons;
    public scr_info_button[] Prop_Array_Info_Buttons
    {
        get{return array_info_buttons;}
        set{array_info_buttons = value;}
    }
    [SerializeField]GameObject How_to_Play_button_object;
    [SerializeField]GameObject Clue_button_object;
    [SerializeField]GameObject Teacher_button_object;
    //[SerializeField]scr_audio_manager ref_audio_manager;
    [SerializeField]scr_audio_manager ref_audio_manager;
    [SerializeField] GameObject x_mark_toggle_music;
    bool only_one_prompt = false;
    [SerializeField]bool toggle_music = true;
    [SerializeField]bool is_complete_level = false;
    [SerializeField]bool is_Played = false;
    [SerializeField]int inf_index;

    void Awake()
    {
        ref_audio_manager = GameObject.FindObjectOfType<scr_audio_manager>();
    }
    void Update()
    {
        Set_Info_Button();
        Set_Prompt_Off1(inf_index);
    }
/*
    public void Set_Prompt_Off()
    {
        if(How_to_Play_prompt.IsActive() || Clue_prompt.IsActive() || Teacher_prompt.IsActive())
        {   Debug.Log("Has Info");
            if(Input.GetMouseButtonDown(0))
            {
                Debug.Log("Has Pressed");
                How_to_Play_prompt.enabled = false;
                Clue_prompt.enabled = false;
                Teacher_prompt.enabled = false;
                only_one_prompt = false;
                How_to_Play_prompt.GetComponent<scr_tween_info_button>().Reduce_Info_Tween();
                Clue_prompt.GetComponent<scr_tween_info_button>().Reduce_Info_Tween();
                Teacher_prompt.GetComponent<scr_tween_info_button>().Reduce_Info_Tween();
            }
        }
    }
*/

    // Set Off Prompt outside of the buttons
    public void Set_Prompt_Off1(int info_index)
    {
        if(array_info_buttons[info_index].prompt.IsActive())
        {
            if(Input.GetMouseButtonDown(0))
            {
                array_info_buttons[info_index].prompt.enabled = false;
                array_info_buttons[info_index].button.enabled = true;
                array_info_buttons[info_index].prompt.GetComponent<scr_tween_info_button>().Reduce_Info_Tween();
                array_info_buttons[info_index].isDisplayed = false;
                only_one_prompt = false;
            }
        }
    }

    public void Info_Button(int info_index)
    {   
        // if the info UI is off
        if(only_one_prompt == false)
        {
            array_info_buttons[info_index].prompt.GetComponent<scr_tween_info_button>().Enlarge_Info_Tween();
            array_info_buttons[info_index].button.enabled = false;
            array_info_buttons[info_index].prompt.enabled = true;
            array_info_buttons[info_index].isDisplayed = true;
            only_one_prompt = true;
            inf_index = info_index;// Assigns it the info_index value for Set_Prompt_Off1
            ref_audio_manager.Play_Audio("info_button_sfx");
        }
        // If the info UI is on
        else if(only_one_prompt == true)
        {
            array_info_buttons[info_index].prompt.GetComponent<scr_tween_info_button>().Reduce_Info_Tween();
            array_info_buttons[info_index].button.enabled = true;
            array_info_buttons[info_index].prompt.enabled = false;
            array_info_buttons[info_index].isDisplayed = false;
            only_one_prompt = false;
        }
    }
    public void Sound()
    {   
        Debug.Log("Sound is Pressed!");
        if(ref_audio_manager.Prop_Toggle_Music == true)
        ref_audio_manager.Prop_Toggle_Music = false;
        else//(if false)
        ref_audio_manager.Prop_Toggle_Music = true;

        if(ref_audio_manager.Prop_Toggle_Music == true)
        {

            if(is_complete_level == false )
            {
                //ref_audio_manager.Play_Audio("bg_audio");
                ref_audio_manager.Play_Audio("bg_audio_sfx");
                //ref_audio_manager.Play_Audio("skate_bg_aud");
                ref_audio_manager.Play_Audio("bg_ambience_audio_sfx");
            }
            else if(is_complete_level == true)
            {
                //ref_audio_manager.Play_Audio("bg_play_every_pg");
                ref_audio_manager.Play_Audio("bg_audio_sfx");
            }

            // Toggle Audio Off
            x_mark_toggle_music.SetActive(false);
        }   
        else// Off
        {
            if(is_complete_level == false)
            {
                //ref_audio_manager.Pause_Audio("bg_audio");
                ref_audio_manager.Pause_Audio("bg_audio_sfx");
                //ref_audio_manager.Pause_Audio("skate_bg_aud");
                ref_audio_manager.Pause_Audio("bg_ambience_audio_sfx");
            }
            else if(is_complete_level == true)
            {
                //ref_audio_manager.Pause_Audio("bg_play_every_pg");
                ref_audio_manager.Pause_Audio("bg_audio_sfx");
            }
            // Toggle Audio On
            x_mark_toggle_music.SetActive(true);
        }
    }

    public void Home()
    {
        Debug.Log("Home is Pressed!");
        //ref_Buttion_Audio.Play();
        //ref_audio_manager.Play_Audio("press_hom_but_aud");
        if(is_Played == true)
        {
            SceneManager.LoadScene(0);
        }
        else
        {   
            ref_audio_manager.Play_Audio("home_sfx");
            ref_audio_manager.Stop_Audio("bg_audio_sfx");
            ref_audio_manager.Stop_Audio("bg_ambience_audio_sfx");
            SceneManager.LoadScene(1);
        }

    }
    // Determine if the Buttons are displayed on screen determine on the type of screen displayed(Dog and Blue)
    void Set_Info_Button()
    {   
        if(ref_yellow_screen.Prop_Yellow_Screen_Object.activeSelf == false && is_complete_level == false)
        {
            How_to_Play_button_object.SetActive(true);
            Clue_button_object.SetActive(true);
            Teacher_button_object.SetActive(true);
        }
        else
        {
            How_to_Play_button_object.SetActive(false);
            Clue_button_object.SetActive(false);
            Teacher_button_object.SetActive(false);
        }
    }

    public GameObject Prop_How_to_Play_button_object
    {
        get{return How_to_Play_button_object;}
        set{How_to_Play_button_object = value;}
    }

    public GameObject Prop_Clue_button_object
    {
        get{return Clue_button_object;}
        set{Clue_button_object = value;}
    }
    public GameObject Prop_Teacher_button_object
    {
        get{return Teacher_button_object;}
        set{Teacher_button_object = value;}
    }

    public bool Prop_Is_Complete_Level
    {
        get{return is_complete_level;}
        set{is_complete_level = value;}
    }
}

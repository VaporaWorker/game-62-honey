﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class scr_tween_base_object:MonoBehaviour
{
    [SerializeField] protected scr_audio_manager ref_audio_manager;
    [SerializeField] protected GameObject tween_object;
    [SerializeField] protected string audio_name;
    [SerializeField] protected float duration;
    [SerializeField] protected float delay;
    
    
}

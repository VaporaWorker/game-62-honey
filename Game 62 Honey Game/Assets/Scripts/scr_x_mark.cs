﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_x_mark : scr_tween_base_object, Iscr_tween_small_scale, Iscr_tween_big_scale
{
    static int inc_index;
    Vector2 scale_big_goal;
    public Vector2 Prop_Scale_Big_Goal
    {
        get{return scale_big_goal;}
        set{scale_big_goal = value;}
    }
    [SerializeField]private Vector2 scale_small_goal;
    public Vector2 Prop_Scale_Small_Goal
    {
        get{return scale_small_goal;}
        set{scale_small_goal = value;}
    }

    void Awake()
    {
        ref_audio_manager = GameObject.FindObjectOfType<scr_audio_manager>();
        tween_object = this.gameObject;
    }

    public void Reduce_Info_Tween()
    {
        // Randomly set a value to determine the audio
        int aud_num = Random.Range(0,3);
        //int aud_vo_num = Random.Range(0,2);
        audio_name = (aud_num == 0) ? "inc_a":(aud_num == 1) ? "inc_b":(aud_num == 2) ? "inc_c" : "no audio match";
        ref_audio_manager.Play_Audio(audio_name);
        LeanTween.scale(tween_object,scale_small_goal,duration);
    }

    public void Enlarge_Info_Tween()
    {
        LeanTween.scale(tween_object,scale_big_goal,duration);
    }

    void OnDisable()
    {
        Enlarge_Info_Tween();
    }

}

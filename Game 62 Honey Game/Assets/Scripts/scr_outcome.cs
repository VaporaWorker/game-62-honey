﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class scr_outcome : MonoBehaviour
{
    [SerializeField]scr_audio_manager ref_audio_manager;
    [SerializeField]scr_x_mark_guess ref_x_guess;
    [SerializeField]scr_check_answer ref_check_answer;
    [SerializeField]scr_card_manager ref_card_manager;
    [SerializeField]scr_info_button_function ref_info_button;
    [SerializeField]scr_ui_popup_function ref_ui_popup;

    void Awake()
    {
        ref_check_answer = this.GetComponent<scr_check_answer>();
        ref_audio_manager = GameObject.FindObjectOfType<scr_audio_manager>();
        
    }

    public IEnumerator Outcome()
    {
        ref_info_button.Prop_Is_Complete_Level = true;
        for(int i = 0; i < ref_info_button.Prop_Array_Info_Buttons.Length; i++)
        {   
            ref_info_button.Prop_Array_Info_Buttons[i].button.enabled = false;
        }
        ref_audio_manager.Stop_Audio("bg_audio_sfx");
        yield return new WaitForSeconds(1.25f);
        ref_audio_manager.Play_Audio("applause_sfx");
        yield return new WaitForSeconds(1.25f);
        ref_audio_manager.Play_Audio("you_did_it_sfx");
        yield return new WaitForSeconds(1.5f);
        ref_audio_manager.Play_Audio("pre_logo_sfx");
        yield return new WaitForSeconds(0.5f);
        ref_audio_manager.Play_Audio("rubber_sfx");
        ref_ui_popup.Prop_Logo_UI.SetActive(true);
        ref_ui_popup.Prop_Logo_UI.GetComponent<scr_tween_scale>().Enlarge_Info_Tween();
        ref_ui_popup.Prop_Genius_UI.SetActive(true);
        ref_ui_popup.Prop_Genius_UI.GetComponent<scr_tween_genius_ui>().Move_Tween_Answer();
        yield return new WaitForSeconds(1.0f);
        ref_audio_manager.Play_Audio("rubber_sfx");
        ref_ui_popup.Prop_Super_UI.SetActive(true);
        ref_ui_popup.Prop_Super_UI.GetComponent<scr_tween_scale>().Reduce_Info_Tween();
        yield return new WaitForSeconds(1.0f);
        ref_audio_manager.Play_Audio("super_genius_sfx");
        yield return new WaitForSeconds(0.75f);
        ref_audio_manager.Play_Audio("congratsulation_sfx");
        /*
        ref_audio_manager.Play_Audio("flyby_card_win_sfx");
        ref_audio_manager.Play_Audio("spin_sfx");
        ref_audio_manager.Play_Audio("rubber_sfx");
        ref_audio_manager.Play_Audio("vo_oh_yeah_sfx");
        ref_audio_manager.Play_Audio("bg_audio_sfx");
        */
    }

    public IEnumerator Outcome(int[] x_placements)
    {
        for(int i = 0; i < x_placements.Length;i++)
        {
            yield return new WaitForSeconds(1.25f);
            
            ref_x_guess.Prop_X_Mark_Yellow[i].SetActive(true); 
            ref_x_guess.Prop_X_Mark_Yellow[i].GetComponent<scr_x_mark>().Reduce_Info_Tween();
            ref_x_guess.Prop_X_Mark_Yellow[i].GetComponent<RectTransform>().anchoredPosition =
            ref_card_manager.Prop_Answer_Choice_Cards[x_placements[i]].GetComponent<RectTransform>().anchoredPosition;
        }


        ref_audio_manager.Play_Audio("second_wrong_answer_sfx");
        yield return new WaitForSeconds(1.75f);
        ref_ui_popup.Prop__Restart_UI.SetActive(true);
        ref_ui_popup.Prop__Restart_UI.GetComponent<scr_restart_stamp>().Reduce_Info_Tween();
        yield return new WaitForSeconds(3.25f);
        SceneManager.LoadScene(0);
        ref_audio_manager.Play_Audio("restart_swoosh_sfx");
        ref_audio_manager.Play_Audio("vo_oh_no_sfx");
        
    }
}

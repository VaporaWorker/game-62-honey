﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_ui_popup_function : MonoBehaviour
{
    [SerializeField] GameObject warning_ui;
    public GameObject Prop_Warning_UI
    {
        get{return warning_ui;}
        set{warning_ui = value;}
    }

    [SerializeField] GameObject restart_ui;
    public GameObject Prop__Restart_UI
    {
        get{return restart_ui;}
        set{restart_ui = value;}
    }
    [SerializeField] GameObject logo_ui;
    public GameObject Prop_Logo_UI
    {
        get{return logo_ui;}
        set{logo_ui = value;}
    }
    [SerializeField] GameObject genius_ui;
    public GameObject Prop_Genius_UI
    {
        get{return genius_ui;}
        set{genius_ui = value;}
    }
    [SerializeField] GameObject super_ui;
    public GameObject Prop_Super_UI
    {
        get{return super_ui;}
        set{super_ui = value;}
    }
    
    public void Activate_Object(GameObject the_object)
    {
        the_object.SetActive(true);
    }

}

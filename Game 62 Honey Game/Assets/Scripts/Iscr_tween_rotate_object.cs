﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Iscr_tween_rotate_object : Iscr_tween_shake_object
{
    float Prop_Rotate_Duration{get;set;}
    void Rotate_Tween();

}

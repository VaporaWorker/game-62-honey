﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scr_warning_ui : scr_tween_base_object, Iscr_tween_big_scale,Iscr_tween_fade_object
{
    [SerializeField] float alpha;
    public float Prop_Alpha
    {
        get{return alpha;}
        set{alpha = value;}
    }
    [SerializeField]float fade_duration;
    public float Prop_Fade_Duration
    {
        get{return fade_duration;}
        set{fade_duration = value;}
    }
    [SerializeField] Vector2 beat_size;
    [SerializeField]private Vector2 scale_big_goal;
    public Vector2 Prop_Scale_Big_Goal
    {
        get{return scale_big_goal;}
        set{scale_big_goal = value;}
    }

    [SerializeField]private Vector2 scale_small_goal;
    public Vector2 Prop_Scale_Small_Goal
    {
        get{return scale_small_goal;}
        set{scale_small_goal = value;}
    }

    void Awake()
    {
        ref_audio_manager = GameObject.FindObjectOfType<scr_audio_manager>();
        tween_object = this.gameObject;
    }

    void OnEnable()
    {
        Enlarge_Info_Tween();
    }

    public void Enlarge_Info_Tween()
    {
        LeanTween.scale(tween_object, scale_big_goal,duration).setOnComplete(Warning_Beat);
    }

    public void Warning_Beat()
    {
        LeanTween.scale(tween_object, beat_size,duration).setLoopPingPong(6).setOnComplete(Fade);
    }

    public void Fade()
    {   
        LeanTween.alpha(tween_object.GetComponent<RectTransform>(),alpha,fade_duration).setOnComplete(Disable_Object);
    }

    void Disable_Object()
    {
        tween_object.SetActive(false);
        LeanTween.scale(tween_object, scale_small_goal,0);
        LeanTween.alpha(tween_object.GetComponent<RectTransform>(),1.0f,0);
        //this.gameObject.GetComponent<Image>().enabled = false;  
    }

}

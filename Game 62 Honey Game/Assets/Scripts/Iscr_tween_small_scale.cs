﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Iscr_tween_small_scale 
{
    Vector2 Prop_Scale_Small_Goal{get;set;}
    void Reduce_Info_Tween();
}

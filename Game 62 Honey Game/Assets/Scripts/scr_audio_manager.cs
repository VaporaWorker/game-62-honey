﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class scr_audio_manager : MonoBehaviour
{
    [SerializeField]bool toggle_music = true;
    [SerializeField]scr_sound[] sound;
    [SerializeField]scr_sound[] correct_vo;
    [SerializeField]int correct_vo_index;
    [SerializeField]scr_sound[] incorrect_vo;
    [SerializeField]int incorrect_vo_index;
    public static scr_audio_manager instance;
    void Awake()
    {
        foreach(scr_sound s in sound)
        {
            s.aud_source = gameObject.AddComponent<AudioSource>();
            s.aud_source.name = s.name;
            s.aud_source.clip = s.clip;
            s.aud_source.volume = s.volume;
            s.aud_source.loop = s.isLoop;
            s.aud_source.playOnAwake = s.isAwake;
        }
        foreach(scr_sound cor_vo in correct_vo)
        {
            cor_vo.aud_source = gameObject.AddComponent<AudioSource>();
            cor_vo.aud_source.name = cor_vo.name;
            cor_vo.aud_source.clip = cor_vo.clip;
            cor_vo.aud_source.volume = cor_vo.volume;
            cor_vo.aud_source.loop = cor_vo.isLoop;
            cor_vo.aud_source.playOnAwake = cor_vo.isAwake;
        }
        foreach(scr_sound incor_vo in incorrect_vo)
        {
            incor_vo.aud_source = gameObject.AddComponent<AudioSource>();
            incor_vo.aud_source.name = incor_vo.name;
            incor_vo.aud_source.clip = incor_vo.clip;
            incor_vo.aud_source.volume = incor_vo.volume;
            incor_vo.aud_source.loop = incor_vo.isLoop;
            incor_vo.aud_source.playOnAwake = incor_vo.isAwake;
        }
        if(instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);
        
    }
    void Start()
    {   
        Play_Audio("bg_audio_sfx");
        Play_Audio("bg_ambience_audio_sfx");
    }

    public void Play_Audio(string name)
    {
        scr_sound s_thing = Array.Find(sound,sounds => sounds.name == name);
        s_thing.aud_source.Play();
    }

    public void Play_Audio(string name, scr_sound[] array_vo)
    {
        scr_sound s_thing = Array.Find(array_vo,array_vos => array_vos.name == name);
        s_thing.aud_source.Play();
    }

    public void Pause_Audio(string name)
    {
        scr_sound s_thing = Array.Find(sound,sounds => sounds.name == name);
        s_thing.aud_source.Pause();
    }
    public void Stop_Audio(String name)
    {
        scr_sound s_thing = Array.Find(sound,sounds => sounds.name == name);
        s_thing.aud_source.Stop();
        
    }

    public bool Prop_Toggle_Music
    {
        get{return toggle_music;}
        set{toggle_music = value;}
    }

    public scr_sound[] Prop_Get_Correct_Vo
    {
        get{return correct_vo;}
    }
    public scr_sound[] Prop_Get_Incorrect_Vo
    {
        get{return incorrect_vo;}
    }

    public int Prop_Correct_Vo_Index
    {
        get{return correct_vo_index;}
        set{correct_vo_index = value;}
    }

    public int Prop_Incorrect_Vo_Index
    {
        get{return incorrect_vo_index;}
        set{incorrect_vo_index = value;}
    }

}
